'use strict';

// let numberToGuess = Math.floor(Math.random() * (20 - 1) + 1);
let numberToGuess = Math.trunc(Math.random() * 20) + 1;
let score = 20;
let highScore = 0;

function testIfGameOver() {
    if (score <= 0) {
        document.querySelector(".message").textContent = "You lost!";
        score = 0;
        document.querySelector(".score").textContent = score;

    } else {
        document.querySelector(".score").textContent = score;
    }
}

function displayMessage(message) {
    document.querySelector(".message").textContent = message;
}

console.log(numberToGuess);

document.querySelector(".check").addEventListener("click", function () {
    let numberInput = Number(document.querySelector(".guess").value);

    if (!numberInput) {
        displayMessage("😢 No number !");

    } else if (numberInput < numberToGuess) {
        displayMessage("Too low!");
        score--;

        testIfGameOver();

    } else if (numberInput > numberToGuess) {
        displayMessage("Too high!");
        score--;

        testIfGameOver();

    } else {
        displayMessage("Correct Number!");
        document.body.style.backgroundColor = "green";
        document.querySelector(".number").textContent = numberToGuess;

        if (score > highScore) {
            highScore = score;
            document.querySelector(".highscore").textContent = highScore;
        }
    }
})

document.querySelector(".again").addEventListener("click", function () {
    numberToGuess = Math.trunc(Math.random() * 20) + 1;
    document.body.style.backgroundColor = "rgb(34, 34, 34)";
    document.querySelector(".guess").value = "";
    displayMessage("Start guessing...");
    document.querySelector(".number").textContent = "?";
    score = 20;
    document.querySelector(".score").textContent = String(score);
})